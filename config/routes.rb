Rails.application.routes.draw do
  root 'pages#home'
  devise_for :users 
  resources :users
  resources :teachers
  resources :semesters
  resources :lessons
  resources :students
  resources :classrooms
  resources :subjects
  resources :courses
  resources :departaments
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

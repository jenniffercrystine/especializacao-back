# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_07_174354) do

  create_table "classrooms", force: :cascade do |t|
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "semester_id"
    t.integer "teacher_id"
    t.string "code"
    t.index ["semester_id"], name: "index_classrooms_on_semester_id"
    t.index ["subject_id"], name: "index_classrooms_on_subject_id"
    t.index ["teacher_id"], name: "index_classrooms_on_teacher_id"
  end

  create_table "course_subjects", force: :cascade do |t|
    t.integer "course_id"
    t.integer "subject_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_course_subjects_on_course_id"
    t.index ["subject_id"], name: "index_course_subjects_on_subject_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.integer "departament_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departament_id"], name: "index_courses_on_departament_id"
  end

  create_table "departaments", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "equivalent_subjects", force: :cascade do |t|
    t.integer "subject_id"
    t.integer "equivalence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["equivalence_id"], name: "index_equivalent_subjects_on_equivalence_id"
    t.index ["subject_id"], name: "index_equivalent_subjects_on_subject_id"
  end

  create_table "lessons", force: :cascade do |t|
    t.string "name"
    t.integer "classroom_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "day"
    t.index ["classroom_id"], name: "index_lessons_on_classroom_id"
  end

  create_table "presences", force: :cascade do |t|
    t.integer "lesson_id"
    t.integer "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["lesson_id"], name: "index_presences_on_lesson_id"
    t.index ["student_id"], name: "index_presences_on_student_id"
  end

  create_table "semesters", force: :cascade do |t|
    t.integer "schoolyear"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "year"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "subscription"
    t.integer "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id"], name: "index_students_on_course_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.integer "departament_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departament_id"], name: "index_subjects_on_departament_id"
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer "classroom_id"
    t.integer "student_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["classroom_id"], name: "index_subscriptions_on_classroom_id"
    t.index ["student_id"], name: "index_subscriptions_on_student_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "name"
    t.integer "registration"
    t.integer "user_id"
    t.integer "departament_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departament_id"], name: "index_teachers_on_departament_id"
    t.index ["user_id"], name: "index_teachers_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.integer "kind"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string "photo"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end

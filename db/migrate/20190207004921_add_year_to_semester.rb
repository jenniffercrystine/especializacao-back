class AddYearToSemester < ActiveRecord::Migration[5.2]
  def change
    add_column :semesters, :year, :datetime
  end
end

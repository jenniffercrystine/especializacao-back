class AddDayToLessons < ActiveRecord::Migration[5.2]
  def change
    add_column :lessons, :day, :string
  end
end

class AddSemesterToClassroom < ActiveRecord::Migration[5.2]
  def change
    add_reference :classrooms, :semester, foreign_key: true
  end
end

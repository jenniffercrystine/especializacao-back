class AddCodeToClassrooms < ActiveRecord::Migration[5.2]
  def change
    add_column :classrooms, :code, :string
  end
end

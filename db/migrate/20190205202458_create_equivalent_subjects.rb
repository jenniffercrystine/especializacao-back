class CreateEquivalentSubjects < ActiveRecord::Migration[5.2]
  def change
    create_table :equivalent_subjects do |t|
      t.references :subject, foreign_key: true
      t.references :equivalence, foreign_key: true

      t.timestamps
    end
  end
end

class ApplicationController < ActionController::Base
    protect_from_forgery
    before_action :authenticate_user!
    
   
    before_action :configure_permitted_parameters, if: :devise_controller?

    # def user_authentication
    #     if user.admin?
    #     elsif user.teacher?
    #     else user.standard?
    #         unless user.id == current_user
    #             flash[:notice] = "Você não possui autorização para isso"
    #             redirect_to current_user
    #         end
            
    #     end
    # end

    

    


    protected
  
    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :kind, :photo])

        devise_parameter_sanitizer.permit(:account_update, keys: [:name, :kind, :photo])
    end
end

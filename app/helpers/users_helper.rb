module UsersHelper
    def user_kind_options
        [['Coordenador', :admin], ['Funcionário Administrativo', :standard], ['Professor', :teacher]]
  end
end

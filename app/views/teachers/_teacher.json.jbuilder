json.extract! teacher, :id, :name, :registration, :user_id, :departament_id, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)

class Teacher < ApplicationRecord
  belongs_to :user
  belongs_to :departament
  has_many :classrooms
  validates :registration, presence: true, uniqueness: true,  length: { is: 9 }
end

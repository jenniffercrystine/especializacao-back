class User < ApplicationRecord
	mount_uploader :photo, PhotoUploader

	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
	devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable
	has_one :teacher
	
	
	validates :kind, presence: true

	delegate :can?, :cannot?, to: :ability
	
	def ability
		@ability ||= Ability.new(self)
	  end

	

	enum kind: {
		standard: 0,
		admin: 1,
		teacher: 2,
	}

	

    
end

class Subject < ApplicationRecord
  belongs_to :departament
  has_many :courses, through: :course_subjects
  has_many :classrooms

  has_many :equivalent_subjects
  has_many :equivalences, :through => :equivalent_subjects
  
  has_many :equivalences, :class_name => "EquivalentSubject", :foreign_key => "equivalence_id"
  has_many :equivalent_subjects, :through => :equivalences, :source => :subject


  
end
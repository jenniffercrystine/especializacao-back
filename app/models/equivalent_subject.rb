class EquivalentSubject < ApplicationRecord
  belongs_to :subject, foreign_key: "subject_id", class_name: "Subject"
  belongs_to :equivalence, foreign_key: "equivalence_id", class_name: "Subject"
end

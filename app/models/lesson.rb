class Lesson < ApplicationRecord
  belongs_to :classroom
  has_many :student, through: :presences
  validates :name, presence: true, uniqueness: true
end

class Student < ApplicationRecord
  belongs_to :course
  has_many :classroom, through: :subscriptions
  has_many :lesson, through: :presences

  validates :subscription, presence: true, uniqueness: true, length: { is: 9 }

  # class Student
  #   self.per_page = 10
  # end
  
end

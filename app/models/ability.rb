class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    # user ||= User.new # guest user (not logged in)
    current_user = user.id
      if user.admin?
        can :manage, :all
      elsif user.teacher?
        can [:index, :show, :create, :destroy, :update], Classroom
        can [:index, :show, :create, :destroy, :update], Lesson
        can [:index, :show, :create], Teacher
        can [:index, :show], Semester
        can [:index, :show], Student
        can [:index, :show], User
        can [:index, :show], Subject
        can [:index, :show], Course
        can [:index, :show], Departament
      elsif user.standard?
        can [:index, :show, :create, :destroy, :update], Classroom
        can [:index, :show, :create, :destroy, :update], Student
        can [:index, :show, :create, :destroy, :update], Semester
        can [:index, :show, :create, :destroy, :update], Subject
        can [:index, :show], User
        can [:index, :show], Teacher
        can [:index, :show], Subject
        can [:index, :show], Departament
        can [:index, :show], Lesson
      end

      
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end

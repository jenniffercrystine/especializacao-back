class Classroom < ApplicationRecord
  belongs_to :subject
  has_many :student, through: :subscriptions
  belongs_to :semester

  validates :code, presence: true, uniqueness: true
end

class Course < ApplicationRecord
  belongs_to :departament
  has_many :subjects, through: :course_subjects
  has_many :students
end
